public class Main {
    public String sample = "sample";
    public static void main(String[] args) {

        //Main newSample = new Main();
        //System.out.println(newSample.sample);
        Car car1 = new Car();
//        System.out.println(car1.brand);
//        System.out.println(car1.make);
//        System.out.println(car1.price);
//        car1.make = "Veyron";
//        car1.brand = "Bugatti";
//        car1.price = 2000000;
//        System.out.println(car1.brand);
//        System.out.println(car1.make);
//        System.out.println(car1.price);
        //Each instance of a class should be independent from one another especially their properties. However, since coming from the same class, they may have the same methods.
        Car car2 = new Car();
//        car2.make = "Tamaraw FX";
//        car2.brand = "Toyota";
//        car2.price = 450000;
//        System.out.println(car2.make);
//        System.out.println(car2.brand);
//        System.out.println(car2.price);
        //car2.owner = "Mang Jose"; - We cannot add a new property not described in the Class.

        /*
            Mini-Activity 1:
            Create a new instance of the Car class and save it in a variable called car3.
            Access the properties of the instance and update its values.
                make = String
                brand = String
                price = int
             You can come up with your own values.
             Print the values of each property of the instance
        */

        //Mini-Activity START
        Car car3 = new Car();
//        car3.make = "Vios";
//        car3.brand = "Toyota";
//        car3.price = 15;
//        System.out.println(car3.make);
//        System.out.println(car3.brand);
//        System.out.println(car3.price);
        //Mini-Activity END
        car3.start();
        car2.start();
        car1.start();
        car1.setMake("Veyron");
        System.out.println(car1.getMake());
        //car1.setBrand("Bugatti");
        car1.setPrice(2000000);
        System.out.println(car1.getBrand());
        System.out.println(car1.getPrice());
        //When creating a new instance of a class:
        //new keyword allows us to create new instance
        //Car() - constructor of our class
        //without arguments - default constructor, which Java can define for us
        Car car4 = new Car();

        Driver driver1 = new Driver("Procopio", 21, "Mandaluyong City");

        System.out.println("Details of driver1:");
        System.out.println(driver1.getName());
        System.out.println(driver1.getAge());
        System.out.println(driver1.getAddress());

        Car car5 = new Car("Corollo", "Toyota", 500000, driver1);
        System.out.println(car5.getMake());
        System.out.println(car5.getBrand());
        System.out.println(car5.getPrice());
        System.out.println(car5.getCarDriverName());

        Animal animal1 = new Animal("Fred", "Orange");
        animal1.call();
    }
}